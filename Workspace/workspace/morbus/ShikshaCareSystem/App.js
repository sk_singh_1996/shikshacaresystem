/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Text, View} from 'react-native';
import ForgetPassword from './src/component/ForgetPassword';
import Faq from './src/component/Faq';
import LoginForm from './src/component/LoginForm';
import Issue from './src/component/Issue';
import Profile from './src/component/Profile';
import Setting from './src/component/Setting';
import ViewAssignment from "./src/component/student/ViewAssignment";
import Announcement from './src/component/student/Announcement';
import ProgressReport from './src/component/student/ProgressReport';
import TimeTable from './src/component/student/TimeTable';
import Library from './src/component/student/Library';
import UploadAssignment from './src/component/teacher/UploadAssignment';
import Attendance from './src/component/teacher/Attendance';
// export default class App extends Component{
//   render() {
//     return <Attendance />;
//   }
// }


// import React from 'react'
import { createDrawerNavigator } from 'react-navigation';
// import Icon from 'react-native-vector-icons/FontAwesome5';
// import Settings from './Settings';  //Tab Nav
// import Profile from './Profile'; //Stack Nav

export default createDrawerNavigator({
  Attendance: {
    screen: Attendance,
    navigationOptions: {
      drawerLabel: "Attendance"
      // drawerIcon: ({ tintColor }) => <Icon name="cog" size={17} />,
    }
  },
  Library: {
    screen: Library,
    navigationOptions: {
      drawerLabel: "Library"
      // drawerIcon: ({ tintColor }) => <Icon name="cog" size={17} />,
    }
  }
});