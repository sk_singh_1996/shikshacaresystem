import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Header, Card, CardSection } from '../common';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

var radio_props = [
    { label: '', value: 0 }
];

class Setting extends Component{
    state={
        value: 0
    }

    render(){
        return(
            <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'space-between',
            }}>

                <View style={{
                    flex: 1,
                    // backgroundColor: 'blue',
                }}>
                    <Header headerText="Settings" />
                    <Card>
                        <CardSection style={{ flexDirection: 'row', alignItems: 'space-between', justifyContent: 'space-between' }}>
                            <Text style={styles.Label}>Notification</Text>
                            <RadioForm
                                style={styles.Radio}
                                radio_props={this.state.types}
                                initial={0}
                                buttonColor={'#EF2D56'}
                                animation={true}
                                onPress={(value) => { this.setState({ value: value }) }}
                                radio_props={radio_props}
                                buttonSize={15}
                            />
                        </CardSection>
                    </Card>
                    <Card>
                        <CardSection style={{ flexDirection: 'row', alignItems: 'space-between', justifyContent: 'space-between' }}>
                            <Text style={styles.Label}>Silent/Vibra</Text>
                            <RadioForm
                                style={styles.Radio}
                                radio_props={this.state.types}
                                initial={0}
                                buttonColor={'#EF2D56'}
                                animation={true}
                                onPress={(value) => { this.setState({ value: value }) }}
                                radio_props={radio_props}
                                buttonSize={15}
                            />
                        </CardSection>
                    </Card>
                    <Card>
                        <CardSection style={{ flexDirection: 'row', alignItems: 'space-between', justifyContent: 'space-between' }}>
                            <Text style={styles.Label}>Light Notify</Text>
                            <RadioForm
                                style={styles.Radio}
                                radio_props={this.state.types}
                                initial={0}
                                buttonColor={'#EF2D56'}
                                animation={true}
                                onPress={(value) => { this.setState({ value: value }) }}
                                radio_props={radio_props}
                                buttonSize={15}
                            />
                        </CardSection>
                    </Card>
                    <Card>
                        <CardSection style={{ flexDirection: 'row', alignItems: 'space-between', justifyContent: 'space-between' }}>
                            <Text style={styles.Label}>Notification</Text>
                            <RadioForm
                                style={styles.Radio}
                                radio_props={this.state.types}
                                initial={0}
                                buttonColor={'#EF2D56'}
                                animation={true}
                                onPress={(value) => { this.setState({ value: value }) }}
                                radio_props={radio_props}
                                buttonSize={15}
                            />
                        </CardSection>
                    </Card>
                </View>
                <View style={{
                    flex: 1,
                    // backgroundColor: 'green',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                    marginBottom: 20,
                }}>
                    <Text style={styles.copyRightText}>CopyRight@ShikshaCareSystem</Text>
                </View>
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        marginTop: 50
    },
    imageStyle: {
        height: 100,
        width: 50,
        flex: 1,
        padding: 50,
        margin: 50
    },
    text: {
        fontSize: 40,
        opacity: 0.5,
        marginLeft: 40,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20
    },
    copyRightText: {
        fontSize: 13,
        color: '#EF2D56'
    },
    textStyle: {
        alignSelf: 'center',
        color: '#fff',
        fontSize: 20,
        // marginLeft: ,
        // marginRight: 5,
        // fontWeight: '600',
        paddingTop: 5,
        paddingBottom: 5,
    },
    buttonStyle: {
        // flex: 1,
        marginTop: 20,
        borderColor: '#EF2D56',
        backgroundColor: '#EF2D56',
        borderWidth: 1,
        borderRadius: 30,
        width: '30%',
        marginLeft: '15%',
        // alignSelf: 'stretch',
        // backgroundColor: '#fff',
        // borderRadius: 5,
        // borderWidth: 1,
        // borderColor: '#007aff',
        // marginLeft: 5,
        // marginRight: 5,
    },
    buttonStyleCancel: {
        // flex: 1,
        marginTop: 20,
        borderColor: '#EF2D56',
        backgroundColor: '#fff',
        borderWidth: 1,
        borderRadius: 30,
        width: '30%',
        marginLeft: '15%',
        // alignSelf: 'stretch',
        // backgroundColor: '#fff',
        // borderRadius: 5,
        // borderWidth: 1,
        // borderColor: '#007aff',
        // marginLeft: 5,
        // marginRight: 5,
    },
    textStyleCancel: {
        alignSelf: 'center',
        color: '#766e6e',
        fontSize: 20,
        // marginLeft: ,
        // marginRight: 5,
        // fontWeight: '600',
        paddingTop: 5,
        paddingBottom: 5,
    },
    Radio: {
        marginLeft: '60%',
        alignItems: 'center'
    },
    Label: {
        fontSize: 20,
    }
};

export default Setting;
