import React, {Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import { Header, Heading } from '../../common';
import { Table, Row, Col, Cell } from "react-native-table-component";
class TimeTable extends Component{

    render = () => {
        return (
            <View style={{ flex: 1, backgroundColor: '#f3f0f0'}}>
                <View style={{flex:1}}>
                    <Header headerText='Time Table' />
                    <Heading>Select Day</Heading>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly'}}>
                        <TouchableOpacity style={styles.buttonSelectedStyle}>
                            <Text style={styles.buttonTextSelectedStyle}>Mon</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonStyle}>
                            <Text style={styles.buttonTestStyle}>Tue</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonStyle}>
                            <Text style={styles.buttonTestStyle}>Wed</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonStyle}>
                            <Text style={styles.buttonTestStyle}>Thu</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonStyle}>
                            <Text style={styles.buttonTestStyle}>Fri</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonStyle}>
                            <Text style={styles.buttonTestStyle}>Sat</Text>
                        </TouchableOpacity>
                    </View>
                    <ScrollView>
                        <View style={{marginTop: 10, justifyContent: 'center', marginLeft: 20, marginRight: 20, borderColor: '#fff', borderWidth: 1, borderLeft: 'none', borderRight:'none', borderRadius: 20, marginBottom: 20}}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 20}}>
                            <Text>Lecture</Text>
                            <Text>Subject</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 10, alignItems: 'center', borderColor: '#fff', borderWidth: 1, borderLeft: 'none', borderRight:'none', borderRadius: 20, borderTopLeftRadius: 0,borderTopRightRadius: 0, borderLeftWidth: 0,borderRightWidth: 0, borderBottomLeftRadius: 0,borderBottomRightRadius: 0, borderBottomWidth: 0}}>
                            <View style={{ marginTop: 10 }}>
                                <Text>Lecture 1</Text>
                                <Text>8:30-9:20</Text>
                            </View>
                            <View style={{ marginTop: 10}}>
                                <Text>English</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 10, alignItems: 'center', borderColor: '#fff', borderWidth: 1, borderLeft: 'none', borderRight:'none', borderRadius: 20, borderTopLeftRadius: 0,borderTopRightRadius: 0, borderLeftWidth: 0,borderRightWidth: 0, borderBottomLeftRadius: 0,borderBottomRightRadius: 0, borderBottomWidth: 0}}>
                            <View style={{ marginTop: 10 }}>
                                <Text>Lecture 1</Text>
                                <Text>8:30-9:20</Text>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Text>English</Text>
                            </View>
                        </View>
                        <View  style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 10, alignItems: 'center', borderColor: '#fff', borderWidth: 1, borderLeft: 'none', borderRight:'none', borderRadius: 20, borderTopLeftRadius: 0,borderTopRightRadius: 0, borderLeftWidth: 0,borderRightWidth: 0, borderBottomLeftRadius: 0,borderBottomRightRadius: 0, borderBottomWidth: 0}}>
                            <View style={{ marginTop: 10 }}>
                                <Text>Lecture 1</Text>
                                <Text>8:30-9:20</Text>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Text>English</Text>
                            </View>
                        </View>
                        <View  style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 10, alignItems: 'center', borderColor: '#fff', borderWidth: 1, borderLeft: 'none', borderRight:'none', borderRadius: 20, borderTopLeftRadius: 0,borderTopRightRadius: 0, borderLeftWidth: 0,borderRightWidth: 0, borderBottomLeftRadius: 0,borderBottomRightRadius: 0, borderBottomWidth: 0}}>
                            <View style={{ marginTop: 10 }}>
                                <Text>Lecture 1</Text>
                                <Text>8:30-9:20</Text>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Text>English</Text>
                            </View>
                        </View>

                        <View  style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 10, alignItems: 'center', borderColor: '#fff', borderWidth: 1, borderLeft: 'none', borderRight:'none', borderRadius: 20, borderTopLeftRadius: 0,borderTopRightRadius: 0, borderLeftWidth: 0,borderRightWidth: 0, borderBottomLeftRadius: 0,borderBottomRightRadius: 0, borderBottomWidth: 0}}>
                            <View style={{ marginTop: 10 }}>
                                <Text style={{fontSize:40, color:'#EF2D56'}}>Lunch</Text>
                                <Text style={{fontSize:25, }}>8:30-9:20</Text>
                            </View>
                        </View>

                        <View  style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 10, alignItems: 'center', borderColor: '#fff', borderWidth: 1, borderLeft: 'none', borderRight:'none', borderRadius: 20, borderTopLeftRadius: 0,borderTopRightRadius: 0, borderLeftWidth: 0,borderRightWidth: 0, borderBottomLeftRadius: 0,borderBottomRightRadius: 0, borderBottomWidth: 0}}>
                            <View style={{ marginTop: 10 }}>
                                <Text>Lecture 1</Text>
                                <Text>8:30-9:20</Text>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Text>English</Text>
                            </View>
                        </View>
                        <View  style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 10, alignItems: 'center', borderColor: '#fff', borderWidth: 1, borderLeft: 'none', borderRight:'none', borderRadius: 20, borderTopLeftRadius: 0,borderTopRightRadius: 0, borderLeftWidth: 0,borderRightWidth: 0, borderBottomLeftRadius: 0,borderBottomRightRadius: 0, borderBottomWidth: 0}}>
                            <View style={{ marginTop: 10 }}>
                                <Text>Lecture 1</Text>
                                <Text>8:30-9:20</Text>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Text>English</Text>
                            </View>
                        </View>
                        <View  style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 10, alignItems: 'center', borderColor: '#fff', borderWidth: 1, borderLeft: 'none', borderRight:'none', borderRadius: 20, borderTopLeftRadius: 0,borderTopRightRadius: 0, borderLeftWidth: 0,borderRightWidth: 0, borderBottomLeftRadius: 0,borderBottomRightRadius: 0, borderBottomWidth: 0}}>
                            <View style={{ marginTop: 10 }}>
                                <Text>Lecture 1</Text>
                                <Text>8:30-9:20</Text>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Text>English</Text>
                            </View>
                        </View>
                        <View  style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 10, alignItems: 'center', borderColor: '#fff', borderWidth: 1, borderLeft: 'none', borderRight:'none', borderRadius: 20, borderTopLeftRadius: 0,borderTopRightRadius: 0, borderLeftWidth: 0,borderRightWidth: 0, borderBottomLeftRadius: 0,borderBottomRightRadius: 0, borderBottomWidth: 0}}>
                            <View style={{ marginTop: 10 }}>
                                <Text>Lecture 1</Text>
                                <Text>8:30-9:20</Text>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Text>English</Text>
                            </View>
                        </View>
                    </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}


const styles = {
    buttonTestStyle: {
        color: '#EF2D56',
        fontWeight: '700',
    },
    buttonTextSelectedStyle: {
        color: '#fff',
        fontWeight: '700',

    },
    buttonSelectedStyle: { 
        marginTop: 5, 
        backgroundColor: '#EF2D56', 
        padding: 10, 
        borderRadius: 18, 
    },
    buttonStyle: {
        marginTop: 5,
        borderColor: '#EF2D56',
        padding: 10,
        borderRadius: 18, 
        borderWidth: 1,
        backgroundColor: '#fff'
    }
}

export default TimeTable;
