import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { Header, Heading, Card, CardSection, Input } from '../../common';
import { createStackNavigator } from "react-navigation";

class Library extends Component{

    static navigationOptions = ({ navigation }) => ({
        title: "Library",
        headerLeft: (
            <TouchableOpacity
                style={{
                    height: 44,
                    width: 44,
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                }}
                onPress={() => navigation.openDrawer()}>
                <Text>Memory</Text>
            </TouchableOpacity>
        ),

    })

    state={
        text: '',
        pass: '',
    }
    render=()=>{
        return (
            <View style={{ flex: 1, backgroundColor: '#f3f0f0'}}>
                <View style={{flex:1}}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 10}}>
                        <TouchableOpacity style={styles.buttonSelectedStyle}>
                            <Text style={styles.buttonTextSelectedStyle}>Search Book</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonStyle}>
                            <Text style={styles.buttonTestStyle}>Request Book</Text>
                        </TouchableOpacity>
                    </View>
                    <ScrollView>
                        <View style={{
                    backgroundColor: '#fff',
                    borderColor: '#fff',
                    borderRadius: 20,
                    borderWidth: 1,
                    padding: 10,
                    paddingTop: 0,
                    margin:10
                    }}>

                    <Card>
                        <CardSection style={styles.containerStyle}>
                            <Input label="Book Name :" value={this.state.text} onChangeText={text => this.setState(
                                { text }
                            )} />
                        </CardSection>

                        <CardSection>
                            <Input label="Auther Name: " value={this.state.pass} onChangeText={pass => this.setState(
                                { pass }
                            )} />
                        </CardSection>

                        <CardSection>
                            <Input label="Subject Name : " value={this.state.pass} onChangeText={pass => this.setState(
                                { pass }
                            )} />
                        </CardSection>

                        <View style={{ flexDirection: 'row', alignItems: 'center',justifyContent: 'space-between', marginLeft:20, marginRight: 20, marginTop: 20}}>
                            <TouchableOpacity style={styles.ViewButtonStyle}>
                                <Text style={styles.buttonTestStyle}>Cancel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.ViewSelectedButtonStyle}>
                                <Text style={styles.buttonTextSelectedStyle}>Search</Text>
                            </TouchableOpacity>
                        </View>
                    </Card>
                </View>
                    <Heading> Available Books </Heading>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginLeft: 20, marginRight: 20, marginTop: 10}}>
                        <Text>English</Text>
                        <TouchableOpacity style={styles.ViewSelectedButtonStyle}>
                            <Text style={styles.buttonSelectedTestStyle}>View</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{marginLeft: 20, marginRight: 20, marginTop: 10, borderColor: '#707070', borderWidth: 1, padding: 10, borderRadius: 20}}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginLeft: 10, marginRight: 10, marginTop: 10}}>
                            <Text>Book Name</Text> 
                            <Text>Book Name</Text>                        
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginLeft: 10, marginRight: 10, marginTop: 10}}>
                            <Text>Book Name</Text> 
                            <Text>Book Name</Text>                        
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginLeft: 10, marginRight: 10, marginTop: 10}}>
                            <Text>Book Name</Text> 
                            <Text>Book Name</Text>                        
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginLeft: 10, marginRight: 10, marginTop: 10}}>
                            <Text>Book Name</Text> 
                            <Text>Book Name</Text>                        
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginLeft: 10, marginRight: 10, marginTop: 10}}>
                            <Text>Book Name</Text> 
                            <Text>Book Name</Text>                        
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginLeft: 20, marginRight: 20, marginTop: 10}}>
                        <Text>Hindi</Text>
                        <TouchableOpacity style={styles.ViewButtonStyle}>
                            <Text style={styles.buttonTestStyle}>View</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginLeft: 20, marginRight: 20, marginTop: 10}}>
                        <Text>Math</Text>
                        <TouchableOpacity style={styles.ViewButtonStyle}>
                            <Text style={styles.buttonTestStyle}>View</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginLeft: 20, marginRight: 20, marginTop: 10}}>
                        <Text>Science</Text>
                        <TouchableOpacity style={styles.ViewButtonStyle}>
                            <Text style={styles.buttonTestStyle}>View</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginLeft: 20, marginRight: 20, marginTop: 10}}>
                        <Text>Social Science</Text>
                        <TouchableOpacity style={styles.ViewButtonStyle}>
                            <Text style={styles.buttonTestStyle}>View</Text>
                        </TouchableOpacity>
                    </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}


const styles = {
    buttonTestStyle: {
        color: '#EF2D56',
        fontWeight: '700',
    },
    buttonTextSelectedStyle: {
        color: '#fff',
        fontWeight: '700',

    },
    buttonSelectedStyle: { 
        marginTop: 5, 
        backgroundColor: '#EF2D56', 
        padding: 10, 
        // width: '30%',
        borderRadius: 18, 
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonStyle: {
        marginTop: 5,
        borderColor: '#EF2D56',
        // width: '30%',
        padding: 10,
        borderRadius: 18, 
        borderWidth: 1,
        backgroundColor: '#fff',
        alignItems: 'center'

    },
    ViewButtonStyle: {
        marginTop: 5,
        borderColor: '#EF2D56',
        padding: 5,
        borderRadius: 18, 
        borderWidth: 1,
        backgroundColor: '#fff',
        alignItems: 'center'

    },
    ViewSelectedButtonStyle: {
        marginTop: 5,
        borderColor: '#EF2D56',
        backgroundColor: '#EF2D56',
        padding: 5,
        borderRadius: 18, 
        borderWidth: 1,
        alignItems: 'center'

    },
    buttonSelectedTestStyle: {
        color: '#fff',
        fontWeight: '700',

    },
}

export default createStackNavigator({
    Library
});