import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Header,Heading } from '../../common';

class ViewAssignment extends Component {

    render=()=>{
        return(
            <View style={{backgroundColor: '#f3f0f0', flex:1}}>
                <Header headerText="View Assignment" />
                <Heading>Latest Assignment</Heading>
                <View style={{backgroundColor: '#ef2d56', padding: 15, marginLeft: 20, marginRight: 20, marginBottom: 20, borderRadius: 10}}>
                    <TouchableOpacity style={{alignItems:'center'}}>
                        <Text style={{color: '#fff', fontWeight: '800'}}>Assignment No. 1</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ backgroundColor: '#fff', padding: 15, marginLeft: 20, marginRight: 20, marginBottom: 20, borderRadius: 10, borderColor: '#ef2d56', borderWidth: 1  }}>
                    <TouchableOpacity style={{ alignItems: 'center' }}>
                        <Text style={{ color: '#000', fontWeight: '800' }}>Assignment No. 2</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ backgroundColor: '#fff', padding: 15, marginLeft: 20, marginRight: 20, marginBottom: 20, borderRadius: 10, borderColor: '#ef2d56', borderWidth: 1  }}>
                    <TouchableOpacity style={{ alignItems: 'center' }}>
                        <Text style={{ color: '#000', fontWeight: '800' }}>Assignment No. 3</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ backgroundColor: '#fff', padding: 15, marginLeft: 20, marginRight: 20, marginBottom: 20, borderRadius: 10}}>
                    <TouchableOpacity style={{ alignItems: 'center' }}>
                        <Text style={{ color: '#000', fontWeight: '800' }}>Assignment No. 4</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

};

export default ViewAssignment;
