import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Header, Heading, Message } from '../../common';

class Announcement extends Component{

    render=()=>{
        return(
            <View style={{backgroundColor: '#f3f0f0', flex: 1}}>
                <Header headerText="Announcement" />
                <Heading>New Announcement</Heading>
                <Message Sender="Mahipal" SendDate="17Nov , 12:30" Msg="Message from teacher for children for holiday" />
                <Message Sender="Mahipal" SendDate="17Nov , 12:30" Msg="Message from teacher for children for holiday" />
                
                <Heading>Old Announcement</Heading>
                <Message Sender="Mahipal" SendDate="17Nov , 12:30" Msg="Message from teacher for children for holiday" />
                <Message Sender="Mahipal" SendDate="17Nov , 12:30" Msg="Message from teacher for children for holiday" />
                <Message Sender="Mahipal" SendDate="17Nov , 12:30" Msg="Message from teacher for children for holiday" />
                <Message Sender="Mahipal" SendDate="17Nov , 12:30" Msg="Message from teacher for children for holiday" />

            </View>
        );
    }

}

export default Announcement;