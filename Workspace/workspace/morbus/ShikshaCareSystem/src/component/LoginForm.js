import React, { Component } from 'react';
import { Image, View, Text } from "react-native";
import { Button, Card, CardSection, Input, LocalImage } from "../common";
import Header from './Header';

class LoginForm extends Component {
    state = {
        text: '',
        pass: '',
    }

    render() {
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'space-between',
            }}>
                <View style={{
                    flex: 1,
                    // backgroundColor: 'red',
                    alignContent: 'center',
                    justifyContent: 'center',
                    marginTop: 100,
                }}>
                    <LocalImage source={require('../resources/img/700px.png')} originalHeight={700} originalWidth={216} />
                    <Text style={styles.text}>Login————</Text>
                </View>
                <View style={{
                    flex: 1,
                    // backgroundColor: 'blue',
                }}>
                    <Card>
                        <CardSection style={styles.containerStyle}>
                            <Input label="Email/Phone" value={this.state.text} onChangeText={text => this.setState(
                                { text }
                            )} />
                        </CardSection>

                        <CardSection>
                            <Input label="Password" value={this.state.pass} onChangeText={pass => this.setState(
                                { pass }
                            )} />
                        </CardSection>

                        <CardSection>
                            <Button onPress={() => alert('loging in.....')}>Log In</Button>
                        </CardSection>
                    </Card>
                </View>
                <View style={{
                    flex: 1,
                    // backgroundColor: 'green',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                    marginBottom: 20,
                }}>
                    <Text style={styles.copyRightText}>CopyRight@ShikshaCareSystem</Text>
                </View>
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        marginTop: 50
    },
    imageStyle: {
        height: 100,
        width: 50,
        flex: 1,
        padding: 50,
        margin: 50
    },
    text: {
        fontSize: 40,
        opacity: 0.5,
        marginLeft: 40,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20
    },
    copyRightText: {
        fontSize: 13,
        color: '#EF2D56'
    }
};

export default LoginForm;
