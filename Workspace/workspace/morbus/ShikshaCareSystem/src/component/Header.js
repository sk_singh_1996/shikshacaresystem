//Import the library needed to make a component
import React from 'react';
import { Text, View } from 'react-native';

//Make a Component
const Header = (props) => {
    const { textStyle, viewStyle } = styles;
    return (
        <View style={viewStyle}>
            <Text style={textStyle} >{props.headerText}</Text>
        </View>
        );
};

const styles = {
    viewStyle: {
        backgroundColor: '#F8F8F9',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        paddingTop: 20,
        // marginTop: 30,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.7,
    },
    textStyle: {
        fontSize: 20
    }
};
//Make that component available to other components
export default Header;
