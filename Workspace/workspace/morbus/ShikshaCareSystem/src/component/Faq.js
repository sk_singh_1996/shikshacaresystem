import React, { Component } from 'react';
import { Image, View, Text, ScrollView, TouchableOpacity } from "react-native";
import { Button, CardList, CardSectionList, Card, CardSection, Input } from "../common";
import Header from './Header';
import LoginForm from './LoginForm';


const Content = ({children}) =>{
    return(
        <View style={{ backgroundColor: '#f4f4f4', marginTop:5, marginLeft: 30, marginRight: 30, padding: 10, borderRadius: 10,flexDirection: 'row', justifyContent: 'space-between' }}>
            {children}
        </View>
    )
}

const Head = ({children}) => {
    return (
        <View style={{ backgroundColor: '#f4f4f4', marginTop: 30, marginLeft: 30, marginRight: 30, padding: 10, borderRadius: 10,flexDirection: 'row', justifyContent: 'space-between' }}>
            {children}
        </View>
    );
};


class Faq extends Component {
    state = {
        text: '',
        pass: '',
    }


    render() {
        return <View style={{ flex: 1, flexDirection: "column", justifyContent: "space-between" }}>
            <Header headerText="FAQ" />
            <View style={{ // flex: ,
                // backgroundColor: 'red',
                alignContent: "center", justifyContent: "center" }
                // backgroundColor: 'red',
            }>
            <ScrollView>
                <Head>
                        <Text style={styles.textQStyle}>1. Why Scm</Text>
                        <TouchableOpacity style={styles.buttonStyleCancel}>
                            <Text style={styles.textStyleCancel}>></Text>
                        </TouchableOpacity>
                </Head>
                <Content>
                    <Text>
                    Lorem Lipsum Text Fillinf the space a brown fox runs so first that it got heart attack.
                    Lorem Lipsum Text Fillinf the space a brown fox runs so first that it got heart attack.
                    Lorem Lipsum Text Fillinf the space a brown fox runs so first that it got heart attack.
                    </Text>
                </Content>
                    <Head>
                        <Text style={styles.textQStyle}>2. What is Scm</Text>
                        <TouchableOpacity style={styles.buttonStyleCancel}>
                            <Text style={styles.textStyleCancel}>></Text>
                        </TouchableOpacity>
                    </Head>
                    <Content>
                        <Text>
                            Lorem Lipsum Text Fillinf the space a brown fox runs so first that it got heart attack.
                    Lorem Lipsum Text Fillinf the space a brown fox runs so first that it got heart attack.
                    </Text>
                    </Content>
                    <Head>
                        <Text style={styles.textQStyle}>3. Working on DataUpdate</Text>
                        <TouchableOpacity style={styles.buttonStyleCancel}>
                            <Text style={styles.textStyleCancel}>></Text>
                        </TouchableOpacity>
                    </Head>
                    <Content>
                        <Text>
                            Lorem Lipsum Text Fillinf the space a brown fox runs so first that it got heart attack.
                    Lorem Lipsum Text Fillinf the space a brown fox runs so first that it got heart attack.
                    o first that it got heart attack
                    </Text>
                    </Content>
                    <Head>
                        <Text style={styles.textQStyle}>Testing</Text>
                        <TouchableOpacity style={styles.buttonStyleCancel}>
                            <Text style={styles.textStyleCancel}>></Text>
                        </TouchableOpacity>
                    </Head>
                    <Content>
                        <Text>
                            Working on it to check if it working properly and
                            checking for some kind of misbehave
                    </Text>
                    </Content>
                    <Head>
                        <Text style={styles.textQStyle}>Testing</Text>
                        <TouchableOpacity style={styles.buttonStyleCancel}>
                            <Text style={styles.textStyleCancel}>></Text>
                        </TouchableOpacity>
                    </Head>
                    <Content>
                        <Text>
                            Working on it to check if it working properly and
                            checking for some kind of misbehave
                    </Text>
                    </Content>
            </ScrollView>
            </View>
            <View style={{ // flex: 1,
                // backgroundColor: 'green',
                justifyContent: "flex-end", alignItems: "center", marginBottom: 20 }}>
            <Text style={styles.copyRightText}>
                CopyRight@ShikshaCareSystem
            </Text>
            </View>
        </View>;
    }
}

const styles = {
    containerStyle: {
        marginTop: 50
    },
    imageStyle: {
        height: 100,
        width: 50,
        flex: 1,
        padding: 50,
        margin: 50
    },
    text: {
        fontSize: 40,
        opacity: 0.5,
        marginLeft: 40,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20
    },
    copyRightText: {
        fontSize: 13,
        color: '#EF2D56'
    },
    containerStyle: {
        marginTop: 50
    },
    imageStyle: {
        height: 100,
        width: 50,
        flex: 1,
        padding: 50,
        margin: 50
    },
    text: {
        fontSize: 40,
        opacity: 0.5,
        marginLeft: 40,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20
    },
    copyRightText: {
        fontSize: 13,
        color: '#EF2D56'
    },
    textStyle: {
        alignSelf: 'center',
        color: '#fff',
        fontSize: 20,
        // marginLeft: ,
        // marginRight: 5,
        // fontWeight: '600',
        paddingTop: 5,
        paddingBottom: 5,
    },
    buttonStyle: {
        // flex: 1,
        marginTop: 20,
        borderColor: '#EF2D56',
        backgroundColor: '#EF2D56',
        borderWidth: 1,
        borderRadius: 30,
        width: '30%',
        marginLeft: '15%',
        // alignSelf: 'stretch',
        // backgroundColor: '#fff',
        // borderRadius: 5,
        // borderWidth: 1,
        // borderColor: '#007aff',
        // marginLeft: 5,
        // marginRight: 5,
    },
    buttonStyleCancel: {
        // flex: 1,
        // marginTop: 20,
        // borderColor: '#EF2D56',
        // backgroundColor: '#fff',
        // borderWidth: 1,
        // borderRadius: 30,
        // width: '30%',
        // marginLeft: '15%',
        // alignSelf: 'stretch',
        // backgroundColor: '#fff',
        // borderRadius: 5,
        // borderWidth: 1,
        // borderColor: '#007aff',
        // marginLeft: 5,
        // marginRight: 5,
    },
    textStyleCancel: {
        alignSelf: 'center',
        fontWeight: '200',
        // color: '#766e6e',
        fontSize: 20,
        transform: [{ rotate: '90deg' }]

        // // marginLeft: ,
        // // marginRight: 5,
        // // fontWeight: '600',
        // paddingTop: 5,
        // paddingBottom: 5,
    },
    textQStyle: {
        alignSelf: 'center',
        fontWeight: '200',
        // color: '#766e6e',
        fontSize: 20,

        // // marginLeft: ,
        // // marginRight: 5,
        // // fontWeight: '600',
        // paddingTop: 5,
        // paddingBottom: 5,
    },
};

export default Faq;
