import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Platform, LayoutAnimation, StyleSheet, TextInput } from "react-native";
import { Header, Heading, LocalImage, Card, CardSection, Input } from '../../common';
import CheckBox from "react-native-checkbox";
import RNPickerSelect from "react-native-picker-select";
import { createStackNavigator } from "react-navigation";


class Attendance extends Component {

    static navigationOptions = ({ navigation }) => ({
        title: "Attendance",
        headerLeft: (
            <TouchableOpacity
                style={{
                    height: 44,
                    width: 44,
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                }}
                onPress={() => navigation.openDrawer()}>
                <Text>Memory</Text>
            </TouchableOpacity>
        ),

    })

    componentWillUpdate(){
        LayoutAnimation.linear();
    }

    renderForm=()=>{
        return(
            <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'space-between',
                backgroundColor: 'f6f0f0'
            }}>
                <View style={{
                    flex: 1,
                    // padding:40,
                    // backgroundColor: 'red',
                    alignContent: 'center',
                    justifyContent: 'center',
                }}>
                    <LocalImage style={{ borderRadius: 50 }} source={require('../../resources/img/apple-icon-152x152.png')} originalHeight={700} originalWidth={216} />
                </View>
                <View style={{
                    flex: 2,
                    // padding:40,
                    // backgroundColor: 'red',
                    alignContent: 'center',
                    justifyContent: 'center',
                }}>
                    <View style={{ paddingVertical: 5 }} />
                    <RNPickerSelect
                        placeholder={{
                            label: 'Select a Class',
                            value: null,
                        }}
                        items={this.state.items}
                        onValueChange={(value) => {
                            this.setState({
                                favColor: value,
                            });
                        }}
                        onUpArrow={() => {
                            this.inputRefs.name.focus();
                        }}
                        onDownArrow={() => {
                            this.inputRefs.picker2.togglePicker();
                        }}
                        style={{ ...pickerSelectStyles }}
                        value={this.state.favColor}
                        ref={(el) => {
                            this.inputRefs.picker = el;
                        }}
                    />
                    <TouchableOpacity style={styles.buttonSelectedStyle}>
                        <Text style={styles.buttonTextSelectedStyle}>Take Attendance</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    onPress = data => this.setState({ data });

    renderList=()=>{
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'space-between',
                backgroundColor: 'f6f0f0',
                marginTop: 20,
            }}>
                <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'space-between',
                backgroundColor: 'f6f0f0',
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginLeft:20,
                marginRight:20,
            }}>
                <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'space-between',
                backgroundColor: 'f6f0f0',
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                marginLeft:20,
                marginRight:20,
                // marginTop: 30,
            }}><Text style={{fontSize: 20}}>Class : </Text><Text style={{color:'#EF2D56', fontSize: 20}}>8th</Text></View>
                <View style={{
                flex: 1,
                flexDirection: 'column',
                backgroundColor: 'f6f0f0',
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                marginLeft:20,
                marginRight:20,
                        // marginTop: 30,
            }}><Text style={{fontSize: 20}}>Date : </Text><Text style={{color:'#EF2D56', fontSize: 20}}>17 Nov</Text></View>
                </View>

                <View style={{
                flex: 6,
                flexDirection: 'column',
                justifyContent: 'space-between',
                backgroundColor: 'f6f0f0'
            }}>
                <ScrollView>
                    <View style={{
                        flexDirection: 'row',
                        backgroundColor: 'f6f0f0',
                        justifyContent: 'space-between',
                        marginLeft:20,
                        marginRight:20,
                        alignItems: 'center',
                        // marginTop: 20,
                    }}>
                    <Text>Name</Text>
                            <CheckBox
                                labelBefore
                                labelStyle={{
                                    display: 'none',
                                }}
                                checked={this.state.checked}
                                onChange={(checked) => this.setState(prev=>({checked: !prev.checked}))}
                            />
                    </View>
                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: 'f6f0f0',
                            justifyContent: 'space-between',
                            marginLeft: 20,
                            marginRight: 20,
                            marginTop: 20,
                        }}>
                            <Text>Name</Text>
                            <CheckBox
                                labelBefore
                                labelStyle={{
                                    display: 'none',
                                }}
                                checked={this.state.checked}
                                onChange={(checked) => this.setState(prev => ({ checked: !prev.checked }))}
                            />
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: 'f6f0f0',
                            justifyContent: 'space-between',
                            marginLeft: 20,
                            marginRight: 20,
                            marginTop: 20,
                        }}>
                            <Text>Name</Text>
                            <CheckBox
                                labelBefore
                                labelStyle={{
                                    display: 'none',
                                }}
                                checked={this.state.checked}
                                onChange={(checked) => this.setState(prev => ({ checked: !prev.checked }))}
                            />
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: 'f6f0f0',
                            justifyContent: 'space-between',
                            marginLeft: 20,
                            marginRight: 20,
                            marginTop: 20,
                        }}>
                            <Text>Name</Text>
                            <CheckBox
                                labelBefore
                                labelStyle={{
                                    display: 'none',
                                }}
                                checked={this.state.checked}
                                onChange={(checked) => this.setState(prev => ({ checked: !prev.checked }))}
                            />
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: 'f6f0f0',
                            justifyContent: 'space-between',
                            marginLeft: 20,
                            marginRight: 20,
                            marginTop: 20,
                        }}>
                            <Text>Name</Text>
                            <CheckBox
                                labelBefore
                                labelStyle={{
                                    display: 'none',
                                }}
                                checked={this.state.checked}
                                onChange={(checked) => this.setState(prev => ({ checked: !prev.checked }))}
                            />
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: 'f6f0f0',
                            justifyContent: 'space-between',
                            marginLeft: 20,
                            marginRight: 20,
                            marginTop: 20,
                        }}>
                            <Text>Name</Text>
                            <CheckBox
                                labelBefore
                                labelStyle={{
                                    display: 'none',
                                }}
                                checked={this.state.checked}
                                onChange={(checked) => this.setState(prev => ({ checked: !prev.checked }))}
                            />
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: 'f6f0f0',
                            justifyContent: 'space-between',
                            marginLeft: 20,
                            marginRight: 20,
                            marginTop: 20,
                        }}>
                            <Text>Name</Text>
                            <CheckBox
                                labelBefore
                                labelStyle={{
                                    display: 'none',
                                }}
                                checked={this.state.checked}
                                onChange={(checked) => this.setState(prev => ({ checked: !prev.checked }))}
                            />
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: 'f6f0f0',
                            justifyContent: 'space-between',
                            marginLeft: 20,
                            marginRight: 20,
                            marginTop: 20,
                        }}>
                            <Text>Name</Text>
                            <CheckBox
                                labelBefore
                                labelStyle={{
                                    display: 'none',
                                }}
                                checked={this.state.checked}
                                onChange={(checked) => this.setState(prev => ({ checked: !prev.checked }))}
                            />
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: 'f6f0f0',
                            justifyContent: 'space-between',
                            marginLeft: 20,
                            marginRight: 20,
                            marginTop: 20,
                        }}>
                            <Text>Name</Text>
                            <CheckBox
                                labelBefore
                                labelStyle={{
                                    display: 'none',
                                }}
                                checked={this.state.checked}
                                onChange={(checked) => this.setState(prev => ({ checked: !prev.checked }))}
                            />
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: 'f6f0f0',
                            justifyContent: 'space-between',
                            marginLeft: 20,
                            marginRight: 20,
                            marginTop: 20,
                        }}>
                            <Text>Name</Text>
                            <CheckBox
                                labelBefore
                                labelStyle={{
                                    display: 'none',
                                }}
                                checked={this.state.checked}
                                onChange={(checked) => this.setState(prev => ({ checked: !prev.checked }))}
                            />
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: 'f6f0f0',
                            justifyContent: 'space-between',
                            marginLeft: 20,
                            marginRight: 20,
                            marginTop: 20,
                        }}>
                            <Text>Name</Text>
                            <CheckBox
                                labelBefore
                                labelStyle={{
                                    display: 'none',
                                }}
                                checked={this.state.checked}
                                onChange={(checked) => this.setState(prev => ({ checked: !prev.checked }))}
                            />
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: 'f6f0f0',
                            justifyContent: 'space-between',
                            marginLeft: 20,
                            marginRight: 20,
                            marginTop: 20,
                        }}>
                            <Text>Name</Text>
                            <CheckBox
                                labelBefore
                                labelStyle={{
                                    display: 'none',
                                }}
                                checked={this.state.checked}
                                onChange={(checked) => this.setState(prev => ({ checked: !prev.checked }))}
                            />
                        </View>
                </ScrollView>
                </View>

                <View style={{
                flex: 2 ,
                flexDirection: 'column',
                justifyContent: 'space-between',
                backgroundColor: 'f6f0f0',
                marginBottom: 20
                }}><View style={{
                flex: 1,

                    flexDirection: 'row',
                    backgroundColor: 'f6f0f0',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginLeft: 20,
                    marginRight: 20,
                    marginTop: 20,
                }}>
                        <View>
                            <TouchableOpacity style={{ alignItems: 'center', backgroundColor: '#fff', padding: 5, borderRadius: 10, borderColor: '#ef2d56', borderWidth: 1, }}>
                                <Text style={{ color: '#000', fontWeight: '200' }}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <TouchableOpacity style={{ alignItems: 'center', backgroundColor: '#EF2D56', padding: 5, borderRadius: 10, borderColor: '#ef2d56', borderWidth: 1, }}>
                                <Text style={{ color: '#fff', fontWeight: '200' }}>Save</Text>
                            </TouchableOpacity>
                        </View>

                    </View> 
                <View style={{
                flex: 1,
                flexDirection: 'row',
                backgroundColor: 'f6f0f0',
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginLeft:20,
                marginRight:20,
            }}><Text style={{fontSize: 20}}>Total Number Of Students :</Text><Text style={{color:'#EF2D56', fontSize: 20}}>60</Text>
            </View>
                <View style={{
                        flex: 1,
                flexDirection: 'row',
                backgroundColor: 'f6f0f0',
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginLeft:20,
                marginRight:20,
            }}><Text style={{fontSize: 20}}>Present :</Text><Text style={{color:'#EF2D56', fontSize: 20}}>45</Text>
                        <Text style={{ fontSize: 20 }}>Absent :</Text><Text style={{ color: '#EF2D56', fontSize: 20 }}>15</Text>
            </View>
            </View>
            </View>
        );
    }

    inputRefs = {};

        state = {
            checked: false,
            value: 0,
            show: false,
            favColor: undefined,
            items: [
                {
                    label: 'Red',
                    value: 'red',
                },
                {
                    label: 'Orange',
                    value: 'orange',
                },
                {
                    label: 'Blue',
                    value: 'blue',
                },
            ],
            favSport: undefined,
            items2: [
                {
                    label: 'Football',
                    value: 'football',
                },
                {
                    label: 'Baseball',
                    value: 'baseball',
                },
                {
                    label: 'Hockey',
                    value: 'hockey',
                },
            ],
        };
    render=()=>{
        console.log(Platform.select({ ios: 'ios-add', android: 'md-add' }));
        return (

            <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'space-between',
                backgroundColor: 'f6f0f0'
            }}>
            {(!this.state.show)?this.renderList():this.renderForm()}
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        marginTop: 50,
        alignContent: 'center'
    },
    imageStyle: {
        height: 100,
        width: 50,
        flex: 1,
        padding: 50,
        margin: 50
    },
    text: {
        fontSize: 40,
        opacity: 0.5,
        marginLeft: 40,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20
    },
    copyRightText: {
        fontSize: 13,
        color: '#EF2D56'
    },
    Name: {
        fontSize: 30,
        color: '#766E6E'
    },
    textStyle: {
        color: '#766E6E'
    },
    ViewSelectedButtonStyle: {
        // marginTop: 5,
        // borderColor: '#EF2D56',
        backgroundColor: '#EF2D56',
        // padding: 5,
        // padding: 10,
        borderRadius: 50,
        // paddingLeft: 5,
        // paddingRight: 5,
        // borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center'

    },
    ViewButtonStyle:{
        // marginTop: 5,
        borderColor: '#EF2D56',
        backgroundColor: '#fff',
        // padding: 5,
        // padding: 10,
        borderRadius: 50,
        // paddingLeft: 5,
        // paddingRight: 5,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonSelectedTestStyle: {
        color: '#fff',
        fontWeight: '700',
        // fontSize:40
        borderRadius: 5,
        
    },
    container: {
        paddingTop: 30,
        backgroundColor: '#fff',
        justifyContent: 'center',
        paddingHorizontal: 10,
    },
    inputIOS: {
        fontSize: 16,
        paddingTop: 13,
        paddingHorizontal: 10,
        paddingBottom: 12,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
    },
    buttonSelectedStyle: { 
        marginTop: 5, 
        backgroundColor: '#EF2D56', 
        padding: 10, 
        // width: '30%',
        borderRadius: 18, 
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    buttonTextSelectedStyle: {
        color: '#fff',
        fontWeight: '700',

    },
    Radio: {
        marginLeft: '60%',
        alignItems: 'center'
    },
};


const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingTop: 13,
        paddingHorizontal: 10,
        paddingBottom: 12,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
    },
});

export default createStackNavigator({
    Attendance
});