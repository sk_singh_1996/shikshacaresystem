import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Platform, LayoutAnimation } from "react-native";
import { Header, Heading, LocalImage, Card, CardSection, Input } from '../../common';

class UploadAssignment extends Component {

    componentWillUpdate(){
        LayoutAnimation.linear();
    }

    state={
        show: false,
    }

    renderForm(){
        if(this.state.show){
            return (
                <View style={{
                    flex: 1,
                    // backgroundColor: 'green',
                    justifyContent: 'center',
                    // alignItems: 'center',
                    // marginBottom: 20,
                }}>
                <Card>
                        <CardSection style={styles.containerStyle}>
                            <Input label="Title :" />
                        </CardSection>
                            <View style={{flexDirection:'row',justifyContent: 'space-between', marginLeft:20, marginRight: 20 }}>
                                <View>
                                <TouchableOpacity style={{ alignItems: 'center',backgroundColor: '#fff', padding: 15, marginTop: 20, borderRadius: 10, borderColor: '#ef2d56', borderWidth: 1, }}>
                                    <Text style={{ color: '#000', fontWeight: '800' }}>Create</Text>
                                </TouchableOpacity>
                                </View>
                                <View>
                                <TouchableOpacity style={{ alignItems: 'center',backgroundColor: '#fff', padding: 15, marginTop: 20, borderRadius: 10, borderColor: '#ef2d56', borderWidth: 1, }}>
                                    <Text style={{ color: '#000', fontWeight: '800' }}>Browse</Text>
                                </TouchableOpacity>
                                </View>
                                <View>
                                <TouchableOpacity style={{ alignItems: 'center',backgroundColor: '#fff', padding: 15, marginTop: 20, borderRadius: 10, borderColor: '#ef2d56', borderWidth: 1, }}>
                                    <Text style={{ color: '#000', fontWeight: '800' }}>Upload</Text>
                                </TouchableOpacity>
                                </View>
                            </View>
                    </Card>
                </View>
            );
        }
    }

    render=()=>{
        console.log(Platform.select({ ios: 'ios-add', android: 'md-add' }));
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'space-between',
                backgroundColor: 'f6f0f0'
            }}><Header headerText="Upload Assignment" />
                
                    {this.renderForm()}
                
                {!this.state.show&&<View style={{
                    flex: 1,
                    // padding:40,
                    // backgroundColor: 'red',
                    alignContent: 'center',
                    justifyContent: 'center',
                }}>
                    <LocalImage style={{ borderRadius: 50 }} source={require('../../resources/img/apple-icon-152x152.png')} originalHeight={700} originalWidth={216} />
                </View>}
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginRight: 20, marginTop: 10 }}>
                    <Heading>New Assignment</Heading>
                    <TouchableOpacity style={styles.ViewSelectedButtonStyle} onPress={()=>{
                        this.setState(prevState=>{
                            return{
                                show: !prevState.show
                            }
                        })
                    }}>
                        <Text style={{
                            color: '#fff',
                            fontWeight: '700',
                            // fontSize:40
                            padding: 5,
                            borderRadius: 5,}}>+</Text>
                    </TouchableOpacity>
                </View>
                
                <View style={{
                    flex: 1,
                    // backgroundColor: 'green',
                    justifyContent: 'flex-end',
                    // alignItems: 'center',
                    marginBottom: 20,
                }}>

                    <Heading>Old Assignment</Heading>
                    <ScrollView>
                        <View>
                            <TouchableOpacity style={{ alignItems: 'center', backgroundColor: '#ef2d56', padding: 15, marginLeft: 20, marginRight: 20, marginBottom: 20, borderRadius: 10 }}>
                                <Text style={{ color: '#fff', fontWeight: '800' }}>Assignment No. 1</Text>
                            </TouchableOpacity>
                        </View>

                        <View>
                            <TouchableOpacity style={{ alignItems: 'center', backgroundColor: '#ef2d56', padding: 15, marginLeft: 20, marginRight: 20, marginBottom: 20, borderRadius: 10 }}>
                                <Text style={{ color: '#fff', fontWeight: '800' }}>Assignment No. 1</Text>
                            </TouchableOpacity>
                        </View>

                        <View>
                            <TouchableOpacity style={{ alignItems: 'center', backgroundColor: '#ef2d56', padding: 15, marginLeft: 20, marginRight: 20, marginBottom: 20, borderRadius: 10 }}>
                                <Text style={{ color: '#fff', fontWeight: '800' }}>Assignment No. 1</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ backgroundColor: '#ef2d56', padding: 15, marginLeft: 20, marginRight: 20, marginBottom: 20, borderRadius: 10 }}>
                            <TouchableOpacity style={{ alignItems: 'center' }}>
                                <Text style={{ color: '#fff', fontWeight: '800' }}>Assignment No. 1</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ backgroundColor: '#fff', padding: 15, marginLeft: 20, marginRight: 20, marginBottom: 20, borderRadius: 10, borderColor: '#ef2d56', borderWidth: 1 }}>
                            <TouchableOpacity style={{ alignItems: 'center' }}>
                                <Text style={{ color: '#000', fontWeight: '800' }}>Assignment No. 2</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ backgroundColor: '#fff', padding: 15, marginLeft: 20, marginRight: 20, marginBottom: 20, borderRadius: 10, borderColor: '#ef2d56', borderWidth: 1 }}>
                            <TouchableOpacity style={{ alignItems: 'center' }}>
                                <Text style={{ color: '#000', fontWeight: '800' }}>Assignment No. 3</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                    <View style={{
                        // flex: 1,
                        // backgroundColor: 'green',
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                        marginBottom: 10,
                    }}>
                        <Text style={styles.copyRightText}>CopyRight@ShikshaCareSystem</Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        marginTop: 50,
        alignContent: 'center'
    },
    imageStyle: {
        height: 100,
        width: 50,
        flex: 1,
        padding: 50,
        margin: 50
    },
    text: {
        fontSize: 40,
        opacity: 0.5,
        marginLeft: 40,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20
    },
    copyRightText: {
        fontSize: 13,
        color: '#EF2D56'
    },
    Name: {
        fontSize: 30,
        color: '#766E6E'
    },
    textStyle: {
        color: '#766E6E'
    },
    ViewSelectedButtonStyle: {
        // marginTop: 5,
        // borderColor: '#EF2D56',
        backgroundColor: '#EF2D56',
        // padding: 5,
        // padding: 10,
        borderRadius: 15,
        paddingLeft: 5,
        paddingRight: 5,
        // borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center'

    },
    buttonSelectedTestStyle: {
        color: '#fff',
        fontWeight: '700',
        // fontSize:40
        borderRadius: 5,
        
    },
};

export default UploadAssignment;