import React, { Component } from 'react';
import { Image, View, Text } from "react-native";
import { Button, Card, CardSection, Input, LocalImage } from "../common";
import Header from './Header';
import LoginForm from './LoginForm';

class Profile extends Component {
    state = {
        text: '',
        pass: '',
    }

    render() {
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'space-between',
            }}><Header headerText="Profile" />
                <View style={{
                    flex: 1,
                    // backgroundColor: 'red',
                    alignContent: 'center',
                    justifyContent: 'center',
                }}>
                    <LocalImage style={{ borderRadius: 50}} source={require('../resources/img/profile.png')} originalHeight={700} originalWidth={216} />
                </View>
                <View style={{
                    flex: 1,
                    // backgroundColor: 'blue',
                    alignItems: 'center',
                    justifyContent: 'space-evenly'
                }}>
                    <Text style={styles.Name}>SUDHANSHU SINGH</Text>
                    <Text style={styles.textStyle}>UserName: SK_S0023</Text>
                    <Text style={styles.textStyle}>Email: shudhanshu88@gmail.com</Text>
                    <Text style={styles.textStyle}>Contact: 9717074214</Text>
                </View>
                <View style={{
                    flex: 1,
                    // backgroundColor: 'green',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                    marginBottom: 20,
                }}>
                    <Text style={styles.copyRightText}>CopyRight@ShikshaCareSystem</Text>
                </View>
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        marginTop: 50,
        alignContent: 'center'
    },
    imageStyle: {
        height: 100,
        width: 50,
        flex: 1,
        padding: 50,
        margin: 50
    },
    text: {
        fontSize: 40,
        opacity: 0.5,
        marginLeft: 40,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20
    },
    copyRightText: {
        fontSize: 13,
        color: '#EF2D56'
    },
    Name: {
        fontSize: 30,
        color: '#766E6E'
    },
    textStyle: {
        color: '#766E6E'
    }
};

export default Profile;
