import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from "react-native";
import { Header, Input, Card, CardSection, Button,InputTextArea } from '../common';
class Issue extends Component{

    state={
        text: '',
        pass: '',
    }

    render=()=>{
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'space-between',
            }}>
                
                <View style={{
                    flex: 1,
                    // backgroundColor: 'blue',
                }}>
                    <Header headerText="Issues" />
                    <Card>
                        <CardSection style={styles.containerStyle}>
                            <Input label="Title :" value={this.state.text} onChangeText={text => this.setState(
                                { text }
                            )} />
                        </CardSection>

                        <CardSection>
                            <InputTextArea label="Description : " value={this.state.pass} onChangeText={pass => this.setState(
                                { pass }
                            )} />
                        </CardSection>

                        <CardSection style={{ flexDirection: 'row', alignItems: 'center',}}>
                            <TouchableOpacity style={styles.buttonStyleCancel}>
                                <Text style={styles.textStyleCancel}>Cancel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.buttonStyle}>
                                <Text style={styles.textStyle}>Send</Text>
                            </TouchableOpacity>
                        </CardSection>
                    </Card>
                </View>
                <View style={{
                    flex: 1,
                    // backgroundColor: 'green',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                    marginBottom: 20,
                }}>
                    <Text style={styles.copyRightText}>CopyRight@ShikshaCareSystem</Text>
                </View>
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        marginTop: 50
    },
    imageStyle: {
        height: 100,
        width: 50,
        flex: 1,
        padding: 50,
        margin: 50
    },
    text: {
        fontSize: 40,
        opacity: 0.5,
        marginLeft: 40,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20
    },
    copyRightText: {
        fontSize: 13,
        color: '#EF2D56'
    },
    textStyle: {
        alignSelf: 'center',
        color: '#fff',
        fontSize: 20,
        // marginLeft: ,
        // marginRight: 5,
        // fontWeight: '600',
        paddingTop: 5,
        paddingBottom: 5,
    },
    buttonStyle: {
        // flex: 1,
        marginTop: 20,
        borderColor: '#EF2D56',
        backgroundColor: '#EF2D56',
        borderWidth: 1,
        borderRadius: 30,
        width: '30%',
        marginLeft: '15%',
        // alignSelf: 'stretch',
        // backgroundColor: '#fff',
        // borderRadius: 5,
        // borderWidth: 1,
        // borderColor: '#007aff',
        // marginLeft: 5,
        // marginRight: 5,
    },
    buttonStyleCancel: {
        // flex: 1,
        marginTop: 20,
        borderColor: '#EF2D56',
        backgroundColor: '#fff',
        borderWidth: 1,
        borderRadius: 30,
        width: '30%',
        marginLeft: '15%',
        // alignSelf: 'stretch',
        // backgroundColor: '#fff',
        // borderRadius: 5,
        // borderWidth: 1,
        // borderColor: '#007aff',
        // marginLeft: 5,
        // marginRight: 5,
    },
    textStyleCancel: {
        alignSelf: 'center',
        color: '#766e6e',
        fontSize: 20,
        // marginLeft: ,
        // marginRight: 5,
        // fontWeight: '600',
        paddingTop: 5,
        paddingBottom: 5,
    },
};
export default Issue;
