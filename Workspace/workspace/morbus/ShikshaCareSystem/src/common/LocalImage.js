import React from 'react';
import { 
    Image,
    Dimensions
} from 'react-native';

const LocalImage = ({ source, originalHeight, originalWidth }) => {
    let windowWidth = Dimensions.get('window').width;
    let widthChange = windowWidth / originalWidth;
    let newWidth =originalWidth * widthChange;
    let windowHeight = Dimensions.get('window').height;
    let heightChange = windowHeight / originalHeight;
    let newHeight = originalHeight * heightChange;
    return(
        <Image
        source={source}
        style={{
            flex: 1,
            width: newWidth,
            height: newHeight,
            resizeMode: 'contain',
            marginTop: 10,
        }}
        />
    );
}

export { LocalImage };
