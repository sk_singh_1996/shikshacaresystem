import React from 'react';
import { TextInput, View, Text, StyleSheet } from 'react-native';
import Textarea from "react-native-textarea";

const InputTextArea = ({ label, value, onChangeText }) => {
    const { inputStyle, labelStyle, containerStyle, inputContainer } = styles1;

    return (
        <View style={containerStyle}>
            <View style={inputContainer}>
                <Text style={labelStyle}>{label}</Text>
                <Textarea
                    containerStyle={styles.textareaContainer}
                    style={styles.textarea}
                    // onChangeText={this.onChange}
                    // defaultValue={this.state.text}
                    maxLength={120}
                    // placeholder={'好玩有趣的，大家同乐，伤感忧闷的，大家同哭。。。'}
                    placeholderTextColor={'#f6f6f6'}
                    underlineColorAndroid={'transparent'}
                />
            </View>
        </View>
    );
};

const styles1 = {
    inputStyle: {
        height: 40,
        width: '85%',
        marginLeft: 30,
        marginRight: 30,
        borderColor: '#f4f4f4',
        fontSize: 20,
        borderWidth: 1,
        borderRadius: 20,
        paddingLeft: 20,
        paddingRight: 20,
    },
    labelStyle: {
        marginLeft: 30,
        marginRight: 30,
        fontSize: 20,
        marginBottom: 20,
        color: '#465353'
    },
    containerStyle: {
        flex: 1,
        width: '100%',
    },
    inputContainer: {
        marginTop: 10,
    },
};
const styles = StyleSheet.create({
    container: {
        // flex: 1,
        padding: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textareaContainer: {
        height: 170,
        marginLeft: 20,
        marginRight: 20,
    },
    textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        width: '90%',
        fontSize: 14,
        color: '#000',
        borderColor: '#f4f4f4',
        borderWidth: 1,
        borderRadius: 20,
        paddingLeft: 20,
        paddingRight: 20,

    },
});

export { InputTextArea };
