import React from 'react';
import { TextInput, View, Text } from 'react-native';

const Input = ({ label, value, onChangeText }) => {
    const { inputStyle, labelStyle, containerStyle, inputContainer } = styles;

    return (
        <View style={containerStyle}>
            <View style={inputContainer}>
                <Text style={labelStyle}>{label}</Text>
                <TextInput
                    style={inputStyle}
                    value={value}
                    onChangeText={onChangeText}
                />
            </View>
        </View>
    );
};

const styles = {
    inputStyle: {
        height: 40,
        width: '85%',
        marginLeft: 30,
        marginRight: 30,
        borderColor: '#f4f4f4',
        fontSize: 20,
        borderWidth: 1,
        borderRadius: 20,
        paddingLeft: 20,
        paddingRight: 20,
    },
    labelStyle:{
        marginLeft: 30,
        marginRight: 30,
        fontSize: 20,
        marginBottom: 20,
        color: '#465353'
    },
    containerStyle: {
        flex: 1,
        width: '100%',
    },
    inputContainer: {
        marginTop: 10,
    },
};

export { Input };
