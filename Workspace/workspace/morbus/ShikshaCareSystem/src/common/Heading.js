import React from 'react';
import { View, Text } from 'react-native';

const Heading = ({children}) => {
    return (
        <View>
            <Text style={{fontSize: 20, color: '#766e6e', margin: 20, fontWeight: 'bold'}}>{children}</Text>
        </View>
    );
};

export { Heading };