import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ onPress, children }) => {
    return (
        <TouchableOpacity onPress={onPress} style={styles.buttonStyle}>
            <Text style={styles.textStyle}>{children}</Text>
        </TouchableOpacity>
    );
};

const styles = {
    textStyle: {
        alignSelf: 'center',
        color: '#fff',
        fontSize: 25,
        marginLeft: 20,
        marginRight: 20,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10,
    },
    buttonStyle: {
        // flex: 1,
        marginTop: 20,
        borderColor: '#EF2D56',
        backgroundColor: '#EF2D56',
        borderWidth: 1,
        borderRadius: 30,
        width: '40%',
        marginLeft: '30%',
        // alignSelf: 'stretch',
        // backgroundColor: '#fff',
        // borderRadius: 5,
        // borderWidth: 1,
        // borderColor: '#007aff',
        // marginLeft: 5,
        // marginRight: 5,
    }
};

export { Button };
