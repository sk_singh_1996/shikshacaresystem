import React from 'react';
import { View, Text } from 'react-native';

const Message = ({ Sender, SendDate, Msg }) => {
    return (
        <View style={{marginLeft: 30,marginTop:10, marginRight: 30, backgroundColor: '#fff',borderRadius: 10}}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between',paddingRight:20, paddingLeft:20, paddingTop:20, borderRadius: 10 }}>
                <Text style={{ color:'#ef2d56', fontWeight:'bold' }}>{Sender}</Text>
                <Text style={{ color:'#ef2d56', fontWeight:'bold' }}>{SendDate}</Text>
            </View>
            <View style={{marginTop: 20,paddingLeft:20,paddingRight: 20, paddingBottom: 20, borderRadius: 10 }}>
                <Text>{Msg}</Text>
            </View>
        </View>
    );
}

export { Message };
